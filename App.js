/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import  React,{Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppContainer from './src/navigation/index'
// import configureStore from './src/store/strore'
// const store = configureStore();
export default class App extends Component {
  render() {
    return (
      //    <Provider store={store}>
     
      // </Provider>
      <AppContainer />
    );
  }
}
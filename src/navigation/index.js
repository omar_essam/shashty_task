import { createAppContainer,
 createSwitchNavigator } from 'react-navigation'
import   {createStackNavigator } from 'react-navigation-stack'
import { TouchableOpacity, Image } from 'react-native'
import   {createDrawerNavigator } from 'react-navigation-drawer'
import   {createMaterialTopTabNavigator } from 'react-navigation-tabs'
import DrawerContainer from './DrawerContainer/DrawerContainer'
import LoginScreen from '../screens/LoginScreen/LoginScreen'
import HomeScreen from '../screens/HomeScreen/HomeScreen'
import React from 'react'
const AppNavigator = createStackNavigator({
    Home: {
      screen: LoginScreen
    },
    RegisterScreen: {
      screen: LoginScreen
    },
  
  });
  const DashboardStackNavigator = createStackNavigator({
    Home: {
      screen: HomeScreen
    }
  }
    ,
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          headerRight:
            <TouchableOpacity
  
              onPress={() => navigation.openDrawer()} >
              <Image
                source={require("../assets/profile_pic.png")}
                style={{
                  width: 40,
                  height: 40,
                  borderRadius: 50,
                  marginLeft: 10,
                  marginTop: 10,
                  paddingBottom:5
                }} />
            </TouchableOpacity>,
   
          headerTitle:(<Image   style={{
            width: 40,
            height: 40,
            borderRadius: 50,
            flex: 1,
            marginTop: 10,
            marginLeft:30,
            paddingBottom:5
          }} 
          resizeMode="contain" 
          source={require("../assets/profile_pic.png")}/>)
  
  
        };
      }
    })
  

  const AppDrawerNavigator = createDrawerNavigator({
    Dashboard: {
      screen: DashboardStackNavigator
    },
  
    ProfileScreen: {
      screen: AppNavigator
    },
  
  },
    {
      contentComponent: ({ navigation }) => (<DrawerContainer navigation={navigation} />),
      drawerPosition  :"right"
  
    }
   )
  
  
  const MainNavigation = createSwitchNavigator({
  
   // AppNavigator: AppNavigator, 
    AppDashboard: AppDrawerNavigator,
  })
      const AppContainer = createAppContainer(MainNavigation)
      export default AppContainer;

import { StyleSheet, Dimensions } from 'react-native'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const DrawerContainerStyle = StyleSheet.create({
  container: {
    backgroundColor: '#0f0f0f',
    flex:1,
  },
  photoContainer: {
    marginTop: 30,
    borderBottomWidth: 1,
    borderColor: "grey",

  },
  innerPhotoContainer: {
    height: height * .25,

    alignItems: "center"
  },

  photo: {
    width: 100,
    height: 100,
    borderRadius: 100,
    marginTop: 10
  },
  userTitleText: {
    fontSize: 17,
    fontWeight: 'bold',
    color: "white",
    marginTop: 8
  },
  searchSection: {

    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    borderColor: 'grey',
  },
  customIcons: {
    marginHorizontal: 10
  },
  input: {
    fontSize: 15,
    paddingLeft: 5,
    color:'white',
  },
})
export default DrawerContainerStyle
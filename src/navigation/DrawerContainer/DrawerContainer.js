import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native'
import CustomIcon from '../../components/customIcon/CustomIcons'
import DrawerContainerStyle from './DrawerContainerStyle'
//import { DrawerActions } from 'react-navigation-drawer';
export default class DrawerContainer extends Component {


    render() {
        const { navigation } = this.props
        const { innerPhotoContainer, photo, photoContainer, userTitleText,container, customIcons, input, searchSection } = DrawerContainerStyle
        return (
            <View style={container}>
                <View style={photoContainer}>
                    <View style={innerPhotoContainer}>

                        <Image
                            source={require("../../assets/profile_pic.png")}
                            style={photo} />

                        <Text style={userTitleText}>Register</Text>
                    </View>
                </View>
                <View style={{alignSelf:"flex-end"}}>
                <TouchableOpacity
                    style={searchSection}
                    onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}
                >
                    <Text style={input}>Home</Text>
                    <CustomIcon
                        type="Ant-design"
                        name="dashboard"
                        color="grey"
                        size={30}
                        style={customIcons}
                    />
             

                </TouchableOpacity>

                <TouchableOpacity
                    style={searchSection}
                    onPress={() => this.props.navigation.navigate('ProfileScreen')}
                >
                    <Text style={input}>Profile</Text>

                    <CustomIcon
                        type="Ant-design"
                        name="profile"
                        color="grey"
                        size={30}
                        style={customIcons}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    style={searchSection}
                    onPress={() =>
                        Alert.alert(
                            'Log out',
                            'Do you want to logout?',
                            [
                                { text: 'Cancel', onPress: () => { return null } },
                                {
                                    text: 'Confirm', onPress: () => {
                                        SharedPreferences.removeItem("userName");
                                        navigation.navigate('AppNavigator')
                                    }
                                },
                            ],
                            { cancelable: false }
                        )
                    }
                >
                    <Text style={input}>Log out</Text>

                    <CustomIcon
                        type="font-awesome"
                        name="sign-out"
                        color="grey"
                        size={30}
                        style={customIcons}
                    />
                </TouchableOpacity>
                </View>
            </View>
        )

    }
}
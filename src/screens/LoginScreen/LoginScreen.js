import React, { Component } from "react";
import {
  View,
  Alert,
  Text,
} from "react-native";


import LoginComponent from "../../components/LoginComponent/LoginComponent";

class LoginScreen extends Component {
  static navigationOptions = {
    title: null,
    header: null
  };


  _checkUserExisting = (allUser, usertoLogin) => {

    for (let user in allUser) {

      if (allUser[user].userName.toLowerCase() === usertoLogin.userName.toLowerCase()
        && allUser[user].password === usertoLogin.password) {
        return true
      }
    }
    return false;
  }



  _submitHandler = (username, password) => {
    if (username !== "" && password !== "") {
      const usertoLogin = {
        userName: username,
        password: password
      }
      //this._getUserData(usertoLogin)
    } else {
      Alert.alert(
        'Warning',
        'please fill your Data',
        [
          { text: 'Ok', onPress: () => { return null } },

        ]
      )
    }

  };
  _onCreateSubmit = () => {
    this.props.navigation.navigate('RegisterScreen')
  }

  render() {
    return (
      <View style={{ flex: 1 }}>

        <LoginComponent onLoginSubmit={this._submitHandler} onCreateSubmit={this._onCreateSubmit} />    
          </View>)
  }
}

export default LoginScreen

import {StyleSheet,Dimensions} from 'react-native'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const HomeScreenStyle=StyleSheet.create({
    container:{
        backgroundColor:"black",
        flex:1
    },
    imageContainer:{
        marginHorizontal:15,
        width:130,
        height:170
    },
    listStyle:{
        marginTop:5
    },
    textStyle:{
        color:"white",
        fontSize:25
    },
    cardView:{
        height:200,
        width:width,
        flex:1,
        marginHorizontal:10,
        marginTop:15
    },
    imageCard:{
        flex:.5,
        width:170,
        height:150
    }
})
export default HomeScreenStyle;
import React, { Component } from "react";
import {
  View,
  Alert,
  FlatList,
  Image,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,Dimensions
} from "react-native";
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import { SliderBox } from 'react-native-image-slider-box';
import { ScrollView } from "react-native-gesture-handler";
import HomeScreenStyle from './HomeScreenStyle'
class HomeScreen extends Component {


constructor(props) {
    super(props);
    this.state = {
      images: [
        'https://source.unsplash.com/1024x768/?nature',
        'https://source.unsplash.com/1024x768/?water',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?tree',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?girl',
      ],
      showCard:false
    };
  }

  _renderCard=function(){
    return(
    this.state.showCard?
    <View style={{    height:200,
      width:width,
      flex:1,
      marginHorizontal:10,
      marginTop:15}}>
    <Image  style={{    flex:.5,
        width:170,
        height:150}}
            source={{uri:'https://source.unsplash.com/1024x768/?nature'}}/>
    </View>
    :
    null
    )

  }
  render() {
    const {container,imageContainer,textStyle,cardView,imageCard}=HomeScreenStyle

    return (
      <View style={container}>

        <ScrollView>
            <SliderBox images={this.state.images} />
            <Text style={textStyle}>الاكثر مشاهدة</Text>
            <FlatList
                horizontal={true}
                data={this.state.images}
                keyExtractor={this._keyExtractor}
              
                renderItem={({ item, index }) => (
                  <TouchableWithoutFeedback 
                  onPress={()=>{this.setState({showCard:true})
                  console.warn(this.state.showCard)}}>
                      <Image  style={imageContainer}
                      source={{uri:item}}/>
                    </TouchableWithoutFeedback>
                )}
            />
            {this._renderCard()}
        </ScrollView>
          </View>
          )
  }
}

export default HomeScreen


import { Dimensions, StyleSheet } from 'react-native'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const LoginComponentStyle = StyleSheet.create({
  welcomeText: {
    fontSize: 22,
    fontWeight: 'bold',
    alignSelf: 'center'

  },
  container: {
    flex: 1,
    marginTop: 20,
    width: width * .9,
    height: height * .9,
    alignSelf: 'center',
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 25,
  },
  imageLogin: {
    width: 200,
    height: 150,
    resizeMode: 'stretch'

  },
  customIcons: {
    marginHorizontal: 10
  },
  input: {

    flex: 1,
    fontSize:17,
    paddingLeft: 5,
    color: '#424242',
  },
  searchSection: {

    borderRadius: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    borderColor: 'black',
    borderWidth: 1,
  },
  searchIcon: {
    padding: 10,
  },

  buttonContainer: {
    height: 50,
    width: 150,
    marginTop: 20,

    backgroundColor: "#33adff",
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 15,
    borderColor: "#c0d4d5",
    alignItems: "center"
  },
  registerText: {
    color: "#33adff",
  },
  registerContainer: {
    justifyContent: 'center',
    marginTop: 20
  }
})
export default LoginComponentStyle;
import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import LoginComponentStyle from "./LoginComponentStyle"
import CustomIcon from "../..//components/customIcon/CustomIcons";

export default class LoginScreen extends Component {
  state = {
    userName: "",
    password: "",
  };

  _userNameChange = val => {
    this.setState({
      userName: val.trim()
    })
  }
  _passwordChange = val => {
    this.setState({
      password: val.trim()
    })
  }
  _submitHandling = () => {
    this.props.onLoginSubmit(this.state.userName, this.state.password)
  }
  _createAccount = () => {
    this.props.onCreateSubmit()
  }
  render() {
    const {
      customIcons,
      searchSection,
      welcomeText,
      imageContainer,
      input,
      imageLogin,
      container,
      registerText,
      registerContainer,
      buttonContainer
    } = LoginComponentStyle;

    return (
      <ScrollView style={container}
      showsVerticalScrollIndicator={false}>
        <Text style={welcomeText}>Welcome</Text>
        <View style={imageContainer}>
          {/* <Image
            source={require("../../assets/twitterlogo.png")}
            style={imageLogin}
          /> */}
        </View>
        <View style={{ marginTop: 70 }}>
          <View style={searchSection}>
            <CustomIcon
              type="Ant-design"
              name="user"
              color="grey"
              size={25}
              style={customIcons}
            />
            <TextInput
              style={input}
              placeholder="UserName"
              keyboardType="visible-password"
              onChangeText={value => {
                this._userNameChange(value)
              }}
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={searchSection}>
            <CustomIcon
              type="Ant-design"
              name="lock1"
              color="grey"
              size={25}
              style={customIcons}
            />
            <TextInput
              style={input}
              placeholder="Password"
              secureTextEntry={true}
              onChangeText={value => {
                this._passwordChange(value)
              }}
              underlineColorAndroid="transparent"
            />
          </View>
        </View>

        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity style={buttonContainer}
            onPress={() => { this._submitHandling() }}>
            <Text style={{ fontWeight: "bold", fontSize: 18, color: "#fff" }}>
              Login
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={registerContainer}
            onPress={() => this._createAccount()}>
            <Text style={registerText}>Create New Account</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

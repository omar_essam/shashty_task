import {applyMiddleware,combineReducers,createStore} from 'redux'
import thunk from 'redux-thunk'
const rootReducers=combineReducers({
    // here compine all reducers
})

const configureStore=()=>{
    return createStore(rootReducers,applyMiddleware(thunk))
}

export default configureStore;